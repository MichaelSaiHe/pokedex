# Pokedex

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.24.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Project Structure

The project consists of two modules: App and Pokemon. Pokemon is a sub-module of App with its own local router.

### App (module)
* Services: Pokedex
* Components: None
* Sub-module: Pokemon

### Pokemon (module)
* Services: None
* Components: Pokemon-list, Pokemon-detail

#### Pokemon-list (component)
Pokemon list handles the output of all pokemon fetched from the pokeapi. It limits rendering to 20 "card" items with each call to the component's loadPokemon() method, unless a larger amount is supplied to the method. 
The component also includes a debounced listener on the search-bar at the top of the page which will result in filtering the elements rendered by loadPokemon(). 

#### Pokemon-detail (component)
Pokemon detail handles the output of a specific pokemon's information. This component is implemented as a child component of pokemon list, which allows it to exist concurrently with its parent. This means that navigating back and forth between the two maintains state effortlessly. 
