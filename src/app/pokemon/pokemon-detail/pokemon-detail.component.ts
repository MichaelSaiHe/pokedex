import { Component, OnInit, Input, OnDestroy, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PokedexService } from '../../pokedex.service';
import { Pokemon } from '../../pokemon';

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.scss']
})

export class PokemonDetailComponent implements OnInit {

  pokemon: Pokemon;
  show = true;


  constructor(
    private pokedexService: PokedexService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {  
  }

  ngOnInit() {
    this.getPokemon();
  }
  
  getPokemon(): void {
    const id = +this.activatedRoute.snapshot.paramMap.get('id');
    this.pokedexService.getPokemonDetails(id)
      .subscribe(pokemon => {
        this.pokemon = pokemon;
        })
  }

  close() {
    this.router.navigateByUrl('/pokemon');
  }

  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.close();
  }
}
