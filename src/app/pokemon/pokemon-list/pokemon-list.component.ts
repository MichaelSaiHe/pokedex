import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

import { Pokemon } from '../../pokemon';

import { PokedexService } from '../../pokedex.service';

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss']
})
export class PokemonListComponent implements OnInit {
  
  loading = true;
  position: number = 0;
  POKEMANS: Pokemon[] = [];
  allPokemonReferences: Pokemon[] = [];

  public searchInput: string; 
  inputUpdate = new Subject<string>(); 

  constructor(private pokedexService: PokedexService) {
    this.inputUpdate
      .pipe(
        debounceTime(500),
        distinctUntilChanged()
      ).subscribe(value => this.search(value));
  }

  ngOnInit() {
    this.getAllPokemonReferences();
  }

  private getAllPokemonReferences(): void {
    this.pokedexService.getPokemonSpecies()
      .subscribe(species => {
        this.pokedexService.getPokemon(species['count'])
          .subscribe(data => {
            this.allPokemonReferences = data['results'];

            let index = 1;
            for(let pokemon of this.allPokemonReferences) {
              Object.assign(pokemon, {id: index++});
            }
            
            this.loadAllPokemon();
          });
      });
  }

  private loadAllPokemon() {
    this.POKEMANS = this.allPokemonReferences; 
    this.loadPokemon();
  }

  private loadPokemon(amount: number = 20): void {
    this.loading = true;

    if(this.position + amount > this.allPokemonReferences.length) {
      this.position = this.allPokemonReferences.length;
    } else {
      this.position += amount; 
    }
    
    this.loading = false;
  }
  
  private search(term: string) {
    this.position = 0; 
    this.POKEMANS = this.allPokemonReferences.filter(p => p.name.toLowerCase().includes(term.toLowerCase()));
    this.loadPokemon();
  }
}
