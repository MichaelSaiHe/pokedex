import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'replace'
})
export class ReplacePipe implements PipeTransform {

  transform(value: string, args: any[]): any {
    for(let key in args) {
      value = value.replace(key, args[key])
    }
    return value;
  }

}
