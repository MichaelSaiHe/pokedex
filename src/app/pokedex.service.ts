import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'; 

import { HttpClient } from '@angular/common/http';

import { Pokemon } from './pokemon'; 

@Injectable({
  providedIn: 'root'
})
export class PokedexService {
  private baseUrl: string = 'https://pokeapi.co/api/v2/pokemon/';

  constructor(private http: HttpClient) { }

  getPokemon(amount: number = 20): Observable<any> {
    return this.http.get(`${this.baseUrl}?limit=${amount}`); 
  }

  getPokemonSpecies(): Observable<any> {
    return this.http.get('https://pokeapi.co/api/v2/pokemon-species/'); 
  }

  getPokemonDetails(id: number): Observable<Pokemon> {
    return this.http.get<Pokemon>(`${this.baseUrl}${id}`)
  }
}
