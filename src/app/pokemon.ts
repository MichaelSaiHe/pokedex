export class Pokemon {
    id: number;
    name: string;
    types: string[];
    abilities;
    sprites;
    weight;
    height;
    stats;
    moves;

    // get getName(){
    //     return this.name.replace('-', ' ');
    // }
    // get GetMoves(){
    //     console.log('moves are:' + this.moves);
    //     let fixedMoves=[];
    //     this.moves.forEach(move => {
    //         console.log(move);
    //         fixedMoves.push(move.move.name.replace('-', ' '));
    //     });
    //     console.log(fixedMoves);
    //     return fixedMoves;
    // }
    
}