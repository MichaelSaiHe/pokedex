'use strict'

/**
 * Dependencies
 * @ignore
 */
const express = require('express')
const morgan = require('morgan')
const history = require('connect-history-api-fallback');


/**
 * App
 * @ignore
 */
const app = express()
app.use(history({
    // verbose: true
}));

app.use(morgan('tiny'))
app.use(express.static('dist/pokedex'))

app.listen(process.env.PORT || 3000, () => console.log(`Listening on port ${process.env.PORT || 3000}`));
